## 파이썬 추천 알고리즘 구현

본 소스코드는 학생이 전공에 맞는 책을 추천하기 위해 작성된 것으로 사용자가
웹 페이지에서 전공을 선택하면 가장 적합한(점수가 높은) 책을 추천한다.
사용자가 추천된 책에 대해 피드백 좋아요 점수를 주면 이 점수를 다음 추천에 적용함으로써
사용자의 피드백이 반영된다.

### 필요한 Python 패키지 설치

- 설치 방법
```
pip install Flask
```

### 참고 자료
- [파이썬 협업필터링(Collaborative Filtering), 추천 알고리즘 - 1](https://kutar37.tistory.com/entry/%ED%8C%8C%EC%9D%B4%EC%8D%AC-%ED%98%91%EC%97%85%ED%95%84%ED%84%B0%EB%A7%81Collaborative-Filtering-%EC%B6%94%EC%B2%9C-%EC%95%8C%EA%B3%A0%EB%A6%AC%EC%A6%98-1)
- [Flash Quickstart](https://flask.palletsprojects.com/en/2.0.x/quickstart/)

