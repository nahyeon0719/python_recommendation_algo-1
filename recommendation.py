# 프로그램에 사용되는 패키지를 import 한다.
import math
import random

# 책과 점수 목록으로 book_score_table 리스트를 만든다.
book_score_table = {}
book_score_table['노인과 바다'] = 0
book_score_table['인터스텔라의 과학'] = 0
book_score_table['Demian'] = 0
book_score_table['군주론'] = 0
book_score_table['미적분으로 바라본 하루'] = 0
book_score_table['백범일지'] = 0
book_score_table['백석 시 전집'] = 0
book_score_table['이기적 유전자'] = 0
book_score_table['화학으로 이루어진 세상'] = 0
book_score_table['The Crucible'] = 0
book_score_table['The giver'] = 0
book_score_table['정의는 무엇인가'] = 0
book_score_table['차라투스트라는 이렇게 말했다'] = 0
book_score_table['세상은 수학이다'] = 0
book_score_table['천재들의 수학 노트'] = 0

# 주어진 score table의 복사본을 만들어 반환한다.
def copy_book_score_table(table):
    result = {}
    for key, value in table.items():
        result[key] = value
    return result

# 학과와 책 점수를 관리하는 데이터를 만든다.
mapping_table = {
    '인문대학': copy_book_score_table(book_score_table),
    '공과대학': copy_book_score_table(book_score_table),
    '자연과학': copy_book_score_table(book_score_table),
    '사회과학': copy_book_score_table(book_score_table),
    '의과대학': copy_book_score_table(book_score_table),
}

def print_mapping_table(table):
    for k, v in table.items():
        print(k, ':', v)

# 사용자가 책을 빌린후 평점을 부여한것을 점수해 반영했다는 가정에서 데이터를 설정해 본다.
mapping_table['인문대학']['노인과 바다'] = 5
mapping_table['인문대학']['인터스텔라의 과학'] = 3
mapping_table['인문대학']['Demian'] = 5
mapping_table['인문대학']['군주론'] = 5
mapping_table['인문대학']['미적분으로 바라본 하루'] = 2

mapping_table['공과대학']['노인과 바다'] = 2
mapping_table['공과대학']['인터스텔라의 과학'] = 5
mapping_table['공과대학']['Demian'] = 2
mapping_table['공과대학']['군주론'] = 1
mapping_table['공과대학']['미적분으로 바라본 하루'] = 5

mapping_table['자연과학']['노인과 바다'] = 3
mapping_table['자연과학']['인터스텔라의 과학'] = 4
mapping_table['자연과학']['Demian'] = 3
mapping_table['자연과학']['군주론'] = 1
mapping_table['자연과학']['미적분으로 바라본 하루'] = 4

# 유클리디안 거리공식을 통한 다차원에서의 두 사람간 거리구하기
# 두값이 크면 클수록 유사도가 높음
def euclidean_distance(data, name1, name2):
    sum=0
    for i in data[name1]:
        if i in data[name2]: # 같은 책을 봤다면
            sum+=math.pow(data[name1][i]- data[name2][i],2)
    return 1/(1+math.sqrt(sum))

# 해당 학과의 해당 책에 대한 점수값을 변경한다.
def update_this_department_book_score(department, book_title, score):
    prev_score = mapping_table[department][book_title]
    new_score = (prev_score + score) / 2
    print('자기 "%s" 학과 "%s" 책 평점 %d 적용: %f --> %f' % (department, book_title, score, prev_score, new_score))
    mapping_table[department][book_title] = new_score

# 타 학과의 해당 책에 대한 점수값을 변경한다.
def update_other_department_book_score(department, book_title, score, sim_value):
    prev_score = mapping_table[department][book_title]
    new_score = (prev_score + score * sim_value) / (1+sim_value)
    print('다른 "%s" 학과 "%s" 책 평점 %d 과 유사도 %f 적용: %f --> %f'
        % (department, book_title, score, sim_value, prev_score, new_score))
    mapping_table[department][book_title] = new_score

# 특정 학과에 적합한 가장 높은 점수순으로 책을 n개 반환한다.
def suggest_top_books(department, n):
    book_score_list = []
    for book_title, score in mapping_table[department].items():
        # 일치하는 book, score dictionary으로 부터 score와 title을 받아서
        # 튜플에 넣는다
        book_score_list.append((book_title, score))
    print('sort 전:', book_score_list)
    book_score_list.sort(key=lambda tup: tup[1], reverse=True)
    print('sort 후:', book_score_list)
    book_list = [tup[0] for tup in book_score_list]  # 목록에서 책 제목만 빼냄
    return book_list[0:n]   # 최종적으로 n개만 빼내어 반환

# 특정 학과내 모든 책들중 exclude_book_titles 목록에 없는 책중 하나를 램덤하게 선택한다.
def suggest_random_book(department, exclude_book_titles):
    book_list = []
    for book_title, score in mapping_table[department].items():
        book_list.append(book_title)
    book_list = set(book_list) - set(exclude_book_titles)
    return random.choice(list(book_list))
