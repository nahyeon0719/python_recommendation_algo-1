function httpGet(path) {
  return fetch(path, getOptions('GET'))
}


function httpPost(path, data) {
  return fetch(path, getOptions('POST', data));
}


function httpPut(path, data) {
  return fetch(path, getOptions('PUT', data));
}


function httpDelete(path) {
  return fetch(path, getOptions('DELETE'));
}

function getOptions(verb, data) {
  var options = {
      dataType: 'json',
      method: verb,
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      }
  };
  if (data) {
      options.body = JSON.stringify(data);
  }
  return options;
}

function getRecommendBooks(depatment) {
  httpGet('/recommend-books/' + depatment)
      .then(response => response.json())
      .then((response) => {
          var books = response.books;
          // Empty the anchor
          var anchor = document.getElementById('recommend-books-anchor');
          anchor.innerHTML = '';
          // Append users to anchor
          books.forEach((book) => {
            anchor.innerHTML += displayBookEle(book);
            anchor.innerHTML += "<br>"
          });
      })
      .catch(e => {
        alert('서버 조회 오류: ' + e);
      })
}

function displayBookEle(book) {
  return `<input name="book" type="radio" value="${book}" onclick="onBookSel()"> ${book}`;
}
